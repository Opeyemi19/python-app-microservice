from app import db


class Guest(db.Model):
    """Simple database model to track event attendees."""

    __tablename__ = 'blacklist'
    id = db.Column(db.Integer, primary_key=True)
    path = db.Column(db.String(80))
    ipaddress = db.Column(db.String(120))
    timestamp = db.Column(db.DateTime)

    def __init__(self, path=None, ipaddress=None, timestamp=None):
        self.path = path
        self.ipaddress = ipaddress
        self.timestamp = timestamp

